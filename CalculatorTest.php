<?php

class calculatorTest extends \PHPUnit\Framework\TestCase
{
    // AAA
    public function test_if_it_sums_correctly(): void
    {
        $a = 1;
        $b = 3;
        $result = 4;

        $calc = new Calculator();
        $actual = $calc->sum($a, $b);

        $this->assertSame($result, $actual);
    }

    public function test_if_it_subtract_correctly(): void
    {
        $a = 5;
        $b = 2;
        $result = 3;

        $calc = new Calculator();
        $actual = $calc->subtract($a, $b);

        $this->assertSame($result, $actual, 'ayirish funksiyasi xato ishladi');
    }

    public function test_if_divides(): void
    {
        $a = 10;
        $b = 2;
        $result = 5;

        $calc = new Calculator();
        $actual = $calc->divide($a, $b);

        $this->assertSame($result, $actual);
    }

    public function test_if_it_throws_exception_when_divider_is_zero(): void
    {
        $a = 3;
        $b = 0;

        $this->expectException(Exception::class);

        $calc = new Calculator();
        $calc->divide($a, $b);
    }

}
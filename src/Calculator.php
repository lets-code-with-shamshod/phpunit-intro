<?php

declare(strict_types=1);

class Calculator
{
    public function sum(int $a, int $b): int
    {
        return $a + $b;
    }

    public function subtract(int $a, int $b): int
    {
        return $a - $b;
    }

    public function divide(int $a, int $b): int|float
    {
        if ($b === 0) {
            throw new Exception('Nolga bo\'lish mumkin emas');
        }

        return $a / $b;
    }
}